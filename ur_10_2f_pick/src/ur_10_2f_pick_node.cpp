#include "ros/ros.h"
#include "sensor_msgs/Range.h"
#include "trajectory_msgs/JointTrajectory.h"
#include "trajectory_msgs/JointTrajectoryPoint.h"
#include "control_msgs/JointTrajectoryControllerState.h"
#include <vector>
#include <string>

float latest_rangefinder_reading;
trajectory_msgs::JointTrajectory msg;
trajectory_msgs::JointTrajectoryPoint trajectory_joint_point;
bool tracking = false;
bool grasped = false;
int acquiring = 0;
int adjust = 100;
double first_detection_distance;
double tracking_distance = 1;
double speed = 0;
double curr_arm_position;

void rangefinderCallback(const sensor_msgs::Range::ConstPtr& msg)
{
    latest_rangefinder_reading = msg->range;
    if (latest_rangefinder_reading < 0.7 && !tracking) { // ignore everything beyond the conveyor
        ROS_ERROR("[UR10_2F_PICK_NODE][RANGE SENSOR] Detected obj @ %s",
                  std::to_string(latest_rangefinder_reading).c_str());
        acquiring += 1;
        first_detection_distance = curr_arm_position;
        return;
    }else if(acquiring > 0 && !tracking){
        tracking = true;
        speed = 0.05 / (0.01 * acquiring); // estimating speed (m/s) from object shape and sensor refresh rate
        ROS_ERROR("[UR10_2F_PICK_NODE][RANGE SENSOR] Estimated conveyor speed: %s", std::to_string(speed).c_str());
    }
}

void armStateCallback(const control_msgs::JointTrajectoryControllerState::ConstPtr& msg)
{
    curr_arm_position = msg->actual.positions[0];
}

void set_arm_traj_msg(const std::vector<double> &target_conf, float time) {
    ros::Duration duration = ros::Duration(time);
    trajectory_joint_point.positions = target_conf;
    trajectory_joint_point.time_from_start = duration;
    msg.points = {trajectory_joint_point};
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "ur10_2f_pick");
    ros::NodeHandle n;

    ROS_ERROR("[UR10_2F_PICK_NODE] Initializing...");

    std::vector<double> conf = {2.20, 0.00, -1.10, 0.00, 5.20, 1.57, 0.00, 0, 0};

    msg.joint_names = {"elbow_joint", "linear_arm_actuator_joint", "shoulder_lift_joint",
                       "shoulder_pan_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint",
                       "r_gripper_prismatic_joint", "l_gripper_prismatic_joint"};

    ros::Publisher trajectory_pub = n.advertise<trajectory_msgs::JointTrajectory>("/ariac/arm/command", 1);
    set_arm_traj_msg(conf, 2.0);

    ros::Rate loop_rate(100);

    ros::Time begin = ros::Time::now();
    ros::Duration init_duration(3);

    while (ros::Time::now() < begin + init_duration){ // works only with rosparam set /use_sim_time false
        trajectory_pub.publish(msg);
        loop_rate.sleep();
    }

    init_duration.sleep(); // to be sure the arm is in home position before activating the rangefinder
    ros::Subscriber rangefinder_sub = n.subscribe("rangefinder", 1, rangefinderCallback);
    ros::Subscriber arm_state_sub = n.subscribe("ariac/arm/state", 1, armStateCallback);

    ROS_ERROR("[UR10_2F_PICK_NODE] Initialization complete!");

    while (ros::ok() && !grasped)
    {

        set_arm_traj_msg(conf, 1);
        trajectory_pub.publish(msg);

        // 0 is the position at which we decide to pick the object, but it can be modified
        if (curr_arm_position <= (first_detection_distance - tracking_distance) && tracking){
            // if you're tracking an object and you're back at the home position, attempt grasp
            ROS_ERROR("[UR10_2F_PICK_NODE] Approaching object and attempting grasp");
            conf = {1.7, (curr_arm_position - (1 * speed)), -0.8, 0.00, 5.37, 1.57, 0.00, 0, 0};
            // close gripper after approaching
            if (curr_arm_position < (first_detection_distance - tracking_distance - (1 * speed))) {
                ROS_ERROR("[UR10_2F_PICK_NODE] Grasping!");
                grasped = true;
                tracking = false;
                break;
            }
        }

        else{
            if (!tracking) {
                // if not tracking, move to waiting position (left side)
                adjust = 100;
                if (curr_arm_position < 2){
                    conf[1] = curr_arm_position + 0.7;
                }
                // stop once waiting position as been reached
                if (curr_arm_position >= 2){
                    conf[1] = curr_arm_position;
                }
            }else{
                // if an object is detected, track it rightward till the home position
                ROS_ERROR("[UR10_2F_PICK_NODE] Tracking");
                if (adjust > 0) adjust--;
                // we need to be faster at the beginning to align with the object center of mass
                conf[1] = curr_arm_position - speed - (0.1 * std::ceil(adjust / 100.0));
            }
        }

        ros::spinOnce();

        loop_rate.sleep();
    }

    ros::Duration end_duration(1);
    begin = ros::Time::now();
    conf[7] = 1;
    conf[8] = 1;

    while(ros::Time::now() < begin + end_duration){
        set_arm_traj_msg(conf, 1);
        trajectory_pub.publish(msg);
        loop_rate.sleep();
    }
    ROS_ERROR("[UR10_2F_PICK_NODE] Grasped!");

    begin = ros::Time::now();
    conf[2] = -1.1;

    while(ros::Time::now() < begin + end_duration){
        set_arm_traj_msg(conf, 1);
        trajectory_pub.publish(msg);
        loop_rate.sleep();
    }

    ROS_ERROR("[UR10_2F_PICK_NODE] Operation complete.");

    return 0;
}